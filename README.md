# Options Selector

This svelte component can help you to create a custom UI selectors

[See the example](https://svelte.dev/repl/a2da73e6367a4047ba9a2b47adf52e0f?version=3.46.4)

## Props
- **value**: selected items (bindable), if the value is an array, the component enables "multiple" selection

- **options**: Array of available options

- **identify**: (optional) Function to get id of options (scalar), it is required if options is an array of objects

- **valueAsId**: A flag to change the value behaviour, if is true the value will be an id or an array of ids

## Slots
- **default** Used to render each element, the slot has the following props
   - **option**: this is an element of "options" prop
   - **selected**: indicated if the element is selected
   - **toggle**: function to toggle the element selection
